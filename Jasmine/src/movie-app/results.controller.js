angular.module('movieApp')
	.controller('ResultsController', function( $location, $exceptionHandler, $log, omdbApi) {

		var vm = this;

		var query = $location.search().q;
		$log.debug('Controller loaded with query: ', query);
		omdbApi.search(query)
			.then(function(data) {
				$log.debug('Data returned for query: ', query, data);
				vm.results = data.Search;
			})
			.catch(function(e) {
				$exceptionHandler(e);
			});

		vm.expand = function expand(index, id) {
			omdbApi.find(id)
				.then(function(data) {
					vm.results[index].data = data;
					vm.results[index].open = true;
				});
		};
	});
